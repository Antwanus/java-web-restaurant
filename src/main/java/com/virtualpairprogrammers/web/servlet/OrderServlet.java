package com.virtualpairprogrammers.web.servlet;

import com.virtualpairprogrammers.data.MenuDataService;
import com.virtualpairprogrammers.domain.MenuItem;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class OrderServlet extends HttpServlet {
   @Override
   protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      resp.setContentType("text/html");
      PrintWriter out = resp.getWriter();
      MenuDataService dataService = new MenuDataService();
      List<MenuItem> fullMenuList = dataService.getFullMenu();

      out.println("<html><body>\n<h1>Ricky's Restaurant</h1><h2>ORDER UP</h2>");
      out.println("<form action='postOrder.html' method='POST'>");
      out.println("<ul>");
      for (MenuItem item :fullMenuList) {
         out.println("<li>"+ item.getName() +" "+ item.getPrice() +"<input type='text' name='item_"+item.getId()+ "'/></li>");
      }

      out.println("</ul><input type='submit'/>");
      out.println("</form>");
      out.println("</body></html>");

      out.close();
   }
}
