package com.virtualpairprogrammers.web.servlet;

import com.virtualpairprogrammers.data.MenuDataService;
import com.virtualpairprogrammers.domain.MenuItem;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class MenuSearchServlet extends HttpServlet {

   @Override
   protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException {
      MenuDataService dataService = new MenuDataService();
      PrintWriter out = response.getWriter();
      response.setContentType("text/html");
      String searchTerm = request.getParameter("searchTerm");
      List<MenuItem> foundMenuItemList = dataService.find(searchTerm);

      out.println("<html><body>\n<h1>Ricky's Restaurant</h1><h2>Search the Menu ("+searchTerm+")</h2>");
      out.println("<ul>");
      if(foundMenuItemList.isEmpty()) out.println("<p>Nothing on the menu containing "+searchTerm+"...</p>");
      else foundMenuItemList.forEach(e-> out.println("<li>"+ e +" "+ e.getDescription() +"</li>"));
      out.println("</ul>");
      out.println("</body></html>");

      out.close();
   }
}
