package com.virtualpairprogrammers.web.servlet;

import com.virtualpairprogrammers.data.MenuDataService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HomePageServlet extends HttpServlet {

   @Override
   protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException {
      MenuDataService dataService = new MenuDataService();
      PrintWriter out = resp.getWriter();
      resp.setContentType("text/html");

      out.println("<html><body>\n<h1>Ricky's Restaurant</h1><h2>Menu</h2>");
      out.println("<ul>");
         dataService.getFullMenu().forEach(menuItem -> out.println("<li>"+ menuItem.toString() + "</li>"));
      out.println("</ul>");
      out.println("<a href='searchResults.html?searchTerm=chicken'>view chicken dishes</a>");
      out.println("</body></html>");

      out.close();
   }
}
