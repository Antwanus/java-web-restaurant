package com.virtualpairprogrammers.web.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class ConfirmOrderServlet extends HttpServlet {
   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      PrintWriter out = resp.getWriter();
      resp.setContentType("text/html");

//      String total = req.getParameter("total");
      HttpSession httpSession = req.getSession();
      Double total = (Double) httpSession.getAttribute("total");
      if (total == null) {
         resp.sendRedirect("/order.html");
         return;
      }
         out.println("<html><body>\n<h1>Ricky's Restaurant</h1><h2>ORDER CONFIRMED, THANK U</h2>");
         out.println("<h5>Total: "+ total +"</h5>");
         out.println("</body></html>");
   }
}
