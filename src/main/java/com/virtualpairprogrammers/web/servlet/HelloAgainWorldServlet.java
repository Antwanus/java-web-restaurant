package com.virtualpairprogrammers.web.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

public class HelloAgainWorldServlet extends HttpServlet {

   @Override
   protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException {
      PrintWriter out = resp.getWriter();
      resp.setContentType("text/html");

      out.println("<html>\n<body>\n<h1>Hello Again World</h1>");
      out.println("<p>" +new Date()+"</p>");
      out.println("</body>\n</html>");

      out.close();
   }
}
