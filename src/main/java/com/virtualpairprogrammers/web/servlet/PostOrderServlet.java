package com.virtualpairprogrammers.web.servlet;

import com.virtualpairprogrammers.data.MenuDataService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class PostOrderServlet extends HttpServlet {
   final MenuDataService menuDataService = new MenuDataService();

   @Override
   public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
      int maxId = menuDataService.getFullMenu().size();
      for (int i = 0; i <maxId; i++) {
         String quantity = request.getParameter("item_" + i);
         try {
            int q = Integer.parseInt(quantity);
            if (q > 0) menuDataService.addToOrder(menuDataService.getItem(i), q);
         }
         catch(NumberFormatException nfe) {
            //that's fine it just means there wasn't an order for this item
         }
      }
      log("New order received!");

      Double total = menuDataService.getOrderTotal();

      HttpSession httpSession = request.getSession();
      httpSession.setAttribute("total", total);
      /* Goes to browser to check if there's a cookie
      *     -> Y: read cookie to find sessionObject from server
      *     -> N: create cookie with sessionID
      * -> server saves sessionObject on server & sends back cookie (maybe updated)
      */
      String redirectUrl = "/confirmOrder.html";
      redirectUrl = response.encodeURL(redirectUrl); // if cookies are disabled, encode the JSESSION into the URL
      response.sendRedirect(redirectUrl);
   }
}
